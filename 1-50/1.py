def add_up(x):
    n = 0

    for i in range(1, x):
        if not i % 5 or not i % 3:
            n = n + i

    print(n)


def main():
    x = 1000
    add_up(x)
    return 0


if __name__ == '__main__':
    main()
