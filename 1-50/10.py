import math

maximum = 2000000


def isprime(n):
    if n == 1:
        return False
    if n < 4:
        return True
    if n % 2 == 0:
        return False
    if n < 9:
        return True
    if n % 3 == 0:
        return False
    else:
        r = math.floor(math.sqrt(n))
        f = 5

        while f <= r:
            if n % f == 0:
                return False
            if n % (f+2) == 0:
                return False
            f += 6

        return True


def main():
    candidate = 1
    vsota = 2

    print("starting")

    while candidate < (maximum-2):
        candidate += 2
        if isprime(candidate):
            vsota += candidate

    print(vsota)


if __name__ == "__main__":
    main()
