import math

maximum = 500


def getdivisors(num):
    divisors = [1, num]

    for i in range(2, math.ceil(math.sqrt(num))):
        if num % i == 0:
            divisors.append(i)
            divisors.append(int(num / i))

    return divisors


def main():
    count = 0
    num = 0

    print("starting")

    while len(getdivisors(num)) <= maximum:
        count += 1
        num += count

    print(num)

if __name__ == "__main__":
    main()
