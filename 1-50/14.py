import time

count = 0
maximum = 1000000
cache = []


def collatz(n):
    global count

    if n == 1:
        return count

    try:
        if cache[n] != 0:
            return count + cache[n] - 1
    except IndexError:
        cache.insert(n, 0)

    if n % 2 == 0:
        n = int(n / 2)
    else:
        n = 3*n + 1

    count += 1

    return collatz(n)


def lencollatz(n):
    global count
    count = 1

    clen = collatz(n)

    try:
        if cache[n] != 0:
            return count + cache[n] - 1
    except IndexError:
        cache.insert(n, clen)

    return clen


def main():
    tnum = 1
    mlen = 0
    mnum = 0

    print("starting")
    start = time.time()

    while tnum < maximum:
        tlen = lencollatz(tnum)

        if tlen > mlen:
            mlen = tlen
            mnum = tnum
            print(mnum, mlen)

        tnum += 1

    print("time=", time.time()-start, "s")

if __name__ == '__main__':
    main()
