maxX = 20
maxY = 20
count = 0

# dela v doglednem času za manj kot 15x15
def premik(x, y):
    if y == maxY and x == maxX: return 1
    
    count = 0

    lahkodesno = x < maxX
    lahkodol = y <= x

    if lahkodesno: count += premik(x + 1, y)
    if lahkodol: count += premik(x, y + 1)

    return count

#print(2*premik(1, 0))


# lažje je z binomsko formulo (izmed 40 izberemo 20)
def fac(n):
    r = 1
    for i in range(n, 1, -1):
        r *= i
    return r

print(fac(40)/fac(20)**2)
