dic = {
    0: 0,
    1: 3,
    2: 3,
    3: 5,
    4: 4,
    5: 4,
    6: 3,
    7: 5,
    8: 5,
    9: 4,
    10: 3,
    11: 6,
    12: 6,
    13: 8,
    14: 8,
    15: 7,
    16: 7,
    17: 9,
    18: 8,
    19: 8,
    20: 6,
    30: 6,
    40: 5,
    50: 5,
    60: 5,
    70: 7,
    80: 6,
    90: 6,
    100: 7
}


def cnt(i):
    count = 0
    i = str(i)

    if len(i) > 2:
        count += dic[int(i[0])]        # hundreds place
        count += 7                     # word 'hundred'
        if i[-2] != '0' or i[-1] != '0':
            count += 3                 # word 'and'
    if len(i) > 1:
        if int(i[-2]) > 1:
            count += dic[int(i[-2] + '0')]  # tens place
            count += dic[int(i[-1])]   # ones place
        else:
            count += dic[int(i[-2:])]  # numbers below 20
    else:
        count += dic[int(i[-1])]

    return count


if __name__ == "__main__":
    c = 0
    for j in range(1, 1000):
        print(str(j) + ' ' + str(cnt(j)))
        c += cnt(j)
    c += 11

    print()
    print(c)
    print()
