def even_sum(x):
    sum = 0
    a, b = 1, 2

    while a < x:
        print(a)
        a, b = b, a + b
        if a % 2 == 0:
            sum += a

    print(sum)


def main():
    x = 4000000
    even_sum(x)
    return 0


if __name__ == '__main__':
    main()
