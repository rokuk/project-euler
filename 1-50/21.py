def sumdivisors(num):
    sum = 1
    delitelj = 2
    while delitelj <= num / 2:
        if num % delitelj == 0:
            sum += delitelj
        delitelj += 1
    return sum


amsum = 0
for a in range(1, 10000):
    suma = sumdivisors(a)
    if sumdivisors(suma) == a and a != suma:
        amsum += a
        print('{} and {} are amicable numbers'.format(a, suma))

print(amsum)
