import time


def main():
    print("starting")
    start = time.time()

    max = pow(10, 999)
    fibbs = [0, 1, 1]

    n = 2

    while fibbs[n] < max:
        n += 1
        fibbs.insert(n, fibbs[n-1] + fibbs[n-2])
        print(n, "-", fibbs[n])

    print(len(str(fibbs[n])))

    print("time=", time.time() - start)


if __name__ == "__main__":
    main()
