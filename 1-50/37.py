def isprime(n):
    n=abs(n)
    if n == 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i ** 2 <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


def prvipogoj(n):
    velja = True
    eksponent = len(str(n)) - 1

    while eksponent >= 0:
        if not isprime(n // 10 ** eksponent):
            velja = False

        eksponent -= 1

    return velja


def drugipogoj(n):
    velja = True
    eksponent = 1

    while eksponent <= len(str(n)):
        if not isprime(n % 10 ** eksponent):
            velja = False

        eksponent += 1

    return velja




primes = []
n = 1000000

for i in range(9, n, 2):
    if isprime(i):
        if prvipogoj(i) and drugipogoj(i):
            primes.append(i)

print(primes)
print(len(primes))
print(sum(primes))
