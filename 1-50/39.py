import math

dict = {}

for i in range(1, 1001):#
    dict[i] = 0

for a in range(1, 500):
    for b in range(1, 501-a):
        c = math.sqrt(a**2 + b**2)
        if c.is_integer():
            dict[a+b+c] += 1

maxval = 0
maxindex = 0

for j in range(1, 1001):
    if dict[j] > maxval:
        maxindex = j
        maxval = dict[j]

print(maxindex, maxval)
