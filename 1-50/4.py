def ispalindrome(num):
    string = str(num)
    palindrome = True

    for count in range(0, len(string)):
        if string[count] != string[len(string) - count - 1]:
            palindrome = False

        count += 1

    return palindrome


def palindromeproduct(min, max):
    palindromes = []

    for i in range(min, max):
        for j in range(min, max):
            if ispalindrome(i*j):
                print(i, "*", j, "=", i*j)
                palindromes.append(i*j)

    palindromes.sort()

    print("Largest palindrome is", palindromes[len(palindromes)-1])


def main():
    palindromeproduct(100, 999)
    return 0


if __name__ == '__main__':
    main()
