import math


def ispentagonal(P):
    D = math.sqrt(1+24*P)
    n1 = (1+D)/6
    n2 = (1-D)/6
    return (n1.is_integer() and n1 > 0) or (n2.is_integer() and n2 > 0)


def getpentagonal(n):
    return n*(3*n-1)/2


n = 10000

# generate 50 pentagonal nums
penums = [0 for i in range(0, n)]

for i in range(1, n+1):
    penums[i-1] = getpentagonal(i)

# check pairs
for j in range(0, n):
    for k in range(j+1, n):
        if ispentagonal(penums[j] + penums[k]) and ispentagonal(penums[k] - penums[j]):
            print(penums[j], penums[k], penums[k] - penums[j])
