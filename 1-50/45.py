import math


def ispentagonal(P):
    D = math.sqrt(1+24*P)
    n1 = (1+D)/6
    n2 = (1-D)/6
    return (n1.is_integer() and n1 > 0) or (n2.is_integer() and n2 > 0)


def ishexagonal(H):
    D = math.sqrt(1+8*H)
    n1 = (1+D)/4
    n2 = (1-D)/4
    return (n1.is_integer() and n1 > 0) or (n2.is_integer() and n2 > 0)


def gettriangle(n):
    return n*(n+1)/2


i = 285
while True:
    i += 1
    n = gettriangle(i)
    print(n)
    if ishexagonal(n) and ispentagonal(n):
        break
