divs = (11, 13, 17, 19)


def isdivisible(x):
    if x % 20 != 0:
        return False

    for div in divs:
        if x % div != 0:
            return False

    if x % 9 != 0:
        return False

    if x % 8 != 0:
        return False

    if x % 7 != 0:
        return False

    if x % 6 != 0:
        return False

    for num in (12, 14, 15, 16, 18, 20):
        if x % num != 0:
            return False

    return True


def smallestmultiple():
    count = 20

    while True:
        if isdivisible(count):
            return count

        count += 1


def main():
    num = smallestmultiple()
    print(num)
    return 0


if __name__ == '__main__':
    main()
