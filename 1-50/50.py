def isprime(n):
    n=abs(n)
    if n == 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i ** 2 <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


primes = [2, 3]
n = 1000000

for i in range(5, n, 2):
    if isprime(i):
        primes.append(i)

lenprimes = len(primes)
maxcount = 1
maxsum = 0




for startindex in range(0, lenprimes):
    print(startindex)
    sum = 0
    count = 0
    while sum < n and startindex + count < lenprimes:
        sum += primes[startindex + count]
        count += 1

        if sum in primes and count > maxcount:
            maxsum = sum
            maxcount = count

print(maxsum, maxcount)
