def squaresum(max):
    num = (max * (max + 1)) / 2
    return num * num


def sumsquares(max):
    sum = 0

    for num in range(1, max + 1):
        square = num * num
        sum += square

    return sum


def main():
    max = 100
    square = squaresum(max)
    sum = sumsquares(max)
    print(square)
    print(sum)
    print(square - sum)
    return 0


if __name__ == '__main__':
    main()
