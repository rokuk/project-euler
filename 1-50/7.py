import math


def isprime(n):
    if n <= 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False

    i = 5
    while i*i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6

    return True


def getnprime(n):
    foundprimes = 0
    count = 0

    while foundprimes < n:
        if isprime(count):
            foundprimes += 1

        count += 1

    return count - 1


def main():
    num = getnprime(10001)
    print(num)
    return 0


if __name__ == '__main__':
    main()
