import math


def main():
    for a in range(0, 993):
        for b in range(0, 993):
            if 1000*(a+b)-a*b == 500000:
                c = math.sqrt(a * a + b * b)
                if a+b+c == 1000 and a < b < c:
                    print(a*b*c)


if __name__ == "__main__":
    main()
