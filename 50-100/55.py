def ispalindrome(sn, ln):
    for j in range(0, int(ln/2)+1):
        if sn[j] != sn[ln-j-1]:
            return False
    return True


def prove(n, i):
    if i == 50:
        return True

    sn = str(n)
    ln = len(sn)

    if ispalindrome(sn, ln) and i != 0:
        return False

    nreverse = 0
    powcount = 0
    for k in sn:
        nreverse += int(k) * 10 ** powcount
        powcount += 1

    return prove(n + nreverse, i + 1)


lychrel = 0

for a in range(1, 10000):
    print(a, lychrel)
    if prove(a, 0):
        lychrel += 1

print(lychrel)
