maxsum = 0

for a in range(2, 100):
    for b in range(1, 100):

        number = str(a ** b)
        suma = 0

        for i in range(0, len(number)):
            suma += int(number[i])

        if suma > maxsum:
            maxsum = suma

print(maxsum)
