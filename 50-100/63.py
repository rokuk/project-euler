import math

count = 0

for i in range(1, 1000):
    for n in range(1, 100):
        p = i ** n

        if len(str(p)) == n: 
            print(i, n, p)
            count += 1

print(count)
