def cycle(n, prva):
    vsota = 0

    for stevka in str(n):
        vsota += int(stevka)**2
    
    if vsota == 89 and prva:
        return 1
    elif vsota == 1 and prva:
        return 0
    elif vsota == 89 or vsota == 1: 
        prva = True

    return cycle(vsota, prva)


count = 0

for i in range(1, 10000000):
    count += cycle(i, False)

print(count)
